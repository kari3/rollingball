import QtQuick 2.0
import VPlay 2.0

EntityBase{
    id: finish

    entityType: "Finish"

    width: gameScene.width *5
    height: 20

    Image{
        id: wallImage
        source: "../../assets/finish.png"
        anchors.fill: finish
    }

    BoxCollider{
        id: finishCollider
        width: parent.width
        height: parent.height -10
        bodyType: Body.Dynamic
        collisionTestingOnlyMode: true //has no physics just collision
    }

    MovementAnimation{ //move until player collides with the finish
        id: movementFinish
        target: finish
        property: "pos"
        velocity: Qt.point(0,-70)
    }

    function die(){ //sets the finish at the starting position again
        finish.x= -gameScene.width *2
        finish.y= gameScene.height+5000
    }

    function startEnd(run){ //starts and ends the movement
        movementFinish.running = run;
        if(!run){
            finish.die()
        }
    }
}


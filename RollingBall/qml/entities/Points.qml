import QtQuick 2.0
import VPlay 2.0
import QtSensors 5.3

EntityBase{
    id: points

    entityType: "Points"

    width: 40
    height: 30

    Image{
        id: pointImage
        source: "../../assets/points.png"

        anchors.fill: points
    }

    BoxCollider{
        id: pointsCollider

        width: parent.width -10
        height: parent.height -10
        bodyType: Body.Dynamic
        collisionTestingOnlyMode: true

        fixture.onBeginContact: {
            var body = other.getBody();
            var target = body.target;
            var otherEntityType = target.entityType; //get the entityType of the collided object

            if(otherEntityType === "Border"){
                points.die()
            }
        }
    }

    MovementAnimation{
        id: movementPoints
        target: points
        property: "pos"
        velocity: Qt.point(0,-80)
    }

    function die(){ //sets the entity to the starting point
        points.x = utils.generateRandomValueBetween(0, gameScene.width)
        points.y = utils.generateRandomValueBetween(gameScene.height-50, gameScene.height +300)
    }

    function startEnd(run){ //starts and ends the movement (and sets the entity on the starting point)
        movementPoints.running = run;
        if(!run){
            points.die()
        }
    }
}


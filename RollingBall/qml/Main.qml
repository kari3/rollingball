import VPlay 2.0
import QtQuick 2.0
import "scenes"

GameWindow {
    id: gameWindow

    width: 640
    height: 960
    activeScene: gameScene

    GameScene {
        id: gameScene
    }
    EntityManager  {
        id: entityManager
        entityContainer: gameScene
    }


}


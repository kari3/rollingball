import QtQuick 2.0
import VPlay 2.0

EntityBase {
    id: border
    entityType: "Border"

    BoxCollider {
        width: gameScene.width * 5 // use large width to make sure the walls and points cannot go by it
        height: 10
        bodyType: Body.Static // this body must not move

        collisionTestingOnlyMode: true
    }
}

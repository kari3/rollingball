    import QtQuick 2.0
import VPlay 2.0
import QtSensors 5.3

EntityBase{
    id: wall

    entityType: "Wall"

    width: 100
    height: 20

    Image{
        id: wallImage
        source: "../../assets/wall.png"

        anchors.fill: wall
    }

    BoxCollider{
        id: wallCollider

        width: parent.width -10
        height: parent.height -10
        bodyType: Body.Dynamic
        collisionTestingOnlyMode: true

        fixture.onContactChanged: {
            var body = other.getBody();
            var target = body.target;
            var otherEntityType = target.entityType;

            if(otherEntityType === "Border"){
                wall.die()
            }
        }
    }

    MovementAnimation{
        id: movementWall
        target: wall
        property: "pos"
        velocity: Qt.point(0,-80)
    }

    function die(){ //sets the entity on the starting point
        wall.x = utils.generateRandomValueBetween(0, gameScene.width)
        wall.y = utils.generateRandomValueBetween(gameScene.height-50, gameScene.height +400)
    }

    function startEnd(run){ //starts and ends the movement
        movementWall.running = run;
        if(!run){
            wall.die()
        }
    }
}


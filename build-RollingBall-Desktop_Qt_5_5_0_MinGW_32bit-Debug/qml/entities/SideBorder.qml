import QtQuick 2.0
import VPlay 2.0

EntityBase { //to block the player for getting too far out of the game window
    id: sideBorder
    entityType: "SideBorder"

    BoxCollider {
        width: 5
        height: gameScene.height
        bodyType: Body.Static

    }
}

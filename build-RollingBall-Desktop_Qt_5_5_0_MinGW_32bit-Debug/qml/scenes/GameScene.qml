import QtQuick 2.0
import VPlay 2.0
import QtSensors 5.5
import "../"
import "../entities"


Scene {
    id: gameScene
    property int score:0
    state: "start"

    onStateChanged: { //to check which state is now
        if(state === "playing"){
            wall.startEnd(true)
            point.startEnd(true)
            finish.startEnd(true)
            var i = 0
            while(pointsRepeater.model >i){
                var pointItem =pointsRepeater.itemAt(i)
                pointItem.startEnd(true)
                i++
            }
            var j = 0
            while(wallRepeater.model >j){
                var wallItem =wallRepeater.itemAt(j)
                wallItem.startEnd(true)
                j++
            }

        }else if(state === "gameOver" | state === "finish"){
            wall.startEnd(false)
            point.startEnd(false)
            finish.startEnd(false)
            var i = 0
            while(pointsRepeater.model >i){ //ends the movement of each entity in the repeater
                var pointItem =pointsRepeater.itemAt(i)
                pointItem.startEnd(false)
                i++
            }

            var j = 0
            while(wallRepeater.model >j){
                var wallItem =wallRepeater.itemAt(j)
                wallItem.startEnd(false)
                j++
            }
        }else if(state === "start"){ //starts the score at 0 again
            gameScene.score =0
        }
    }


    // actual scene size
    width: 320
    height: 480

    Keys.forwardTo: player.controller

    Accelerometer{
        id: accelerometer
        active: true
    }
    PhysicsWorld {
        debugDrawVisible: false
        updatesPerSecondForPhysics: 60
    }


    Image{ //the background
        anchors.fill: parent.gameWindowAnchorItem
        source: "../../assets/background.png"
    }

    Image{ //the scoring background
        id: scoreCounter
        source: "../../assets/scoring.png"

        Text{ //displays the score
            id: scoreText
            anchors.centerIn: parent
            color: "black"
            font.pixelSize: 12
            text: score
        }
    }

    Border{ //so that the wall and points collide at the end and then they start again
        id: topBorder
        x: 0
        y: 0
    }

    SideBorder{ //to block the player left and right from going out of the gameWindow
        id: leftBorder
        x: 0 - 50
        y: 0
    }

    SideBorder{
        id: rightBorder
        x: gameScene.width + 50
        y: 0
    }

    Finish{ //the finish line
        id: finish
        x: -gameScene.width *2
        y: gameScene.height+5000
    }

    Wall{
        id: wall
        x: utils.generateRandomValueBetween(0, gameScene.width)
        y: utils.generateRandomValueBetween(gameScene.height-50, gameScene.height +100)
    }

    Points{
        id: point
        x: utils.generateRandomValueBetween(0, gameScene.width)
        y: utils.generateRandomValueBetween(gameScene.height-50, gameScene.height+100)
    }

    Repeater{ //creates more walls
        id: wallRepeater
        model: 3
        Wall{
            x: utils.generateRandomValueBetween(0, gameScene.width)
            y: utils.generateRandomValueBetween(gameScene.height-50, gameScene.height+400)

        }

    }

    Repeater{ //creates more flowers
        id: pointsRepeater
        model: 2
        Points{
            x: utils.generateRandomValueBetween(0, gameScene.width)
            y: utils.generateRandomValueBetween(gameScene.height-50, gameScene.height+300)
        }
    }


    Player{
        id: player
        x: gameScene.width/2
        y: 90
    }

    MouseArea{ //for the whole screen for finishing and starting
        id: mouseArea
        anchors.fill: gameScene.gameWindowAnchorItem
        onClicked:{
            if(gameScene.state === "start"){
                gameScene.state = "playing"
            }else if(gameScene.state == "gameOver" | gameScene.state == "finish"){
                gameScene.state = "start"
            }
        }
    }

    Image{ //overlaying image for start, gameOver and finsh
        id: overlayingInfo
        anchors.centerIn: parent
        source: {
            if(gameScene.state === "gameOver"){
                "../../assets/gameOver.png"
            }else if(gameScene.state === "start") {
                "../../assets/startClick.png"
            }else{
                "../../assets/finished.png"
            }
        }
        visible: gameScene.state !== "playing"
        Text{ //shows the score when the player is finished
            visible: (gameScene.state === "finish")
            text: "Score: "+gameScene.score
            anchors.centerIn: overlayingInfo
            color: "black"
            font.bold: true
        }
    }

}

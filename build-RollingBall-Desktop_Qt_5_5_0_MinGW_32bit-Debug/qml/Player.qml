import QtQuick 2.0
import VPlay 2.0
import QtSensors 5.3

EntityBase {
    id: playerEntity
    entityType: "Player"

    property alias controller: twoAxisController

    TwoAxisController{ //to move the ball to the left or the right
        id: twoAxisController
    }

    SpriteSequenceVPlay {
        id: playerAnimation
        running: gameScene.state === "playing" ? true :false

        defaultSource: "../assets/player.png"

        SpriteVPlay { //just one sprite to play
            id: rolling
            name: "rolling"
            frameWidth: 93
            frameHeight: 95
            startFrameColumn: 1
            startFrameRow: 2

            frameCount: 5
            frameRate: 14

            frameX: 0
            frameY: 0

        }

    }

    CircleCollider{
        id: playerCollider

        bodyType: Body.Dynamic
        radius: (rolling.frameWidth) /2
        visible: false
        anchors.left: playerAnimation.left //to have the animation and the collider at the same position
        anchors.top: playerAnimation.top

        linearVelocity.x: system.desktopPlatform ?
                              twoAxisController.xAxis * 200 :
                              (accelerometer.reading !== null ? -accelerometer.reading.x * 100 : 0)

        fixture.onBeginContact: { //what to do when the player collides with something

            if(gameScene.state === "playing"){
                var body = other.getBody();
                var target = body.target;
                var otherEntityType = target.entityType;

                if(otherEntityType === "Finish"){ //collision action depends on the entity type
                    playerEntity.finished()
                }
                else if(otherEntityType === "Wall"){
                    playerEntity.die()
                    target.die()
                }else if(otherEntityType === "Points"){
                    target.die()
                    gameScene.score++
                }
            }


        }
    }

    function finished(){ //when the player hit the finish
        playerEntity.x = gameScene.width / 2
        playerEntity.y = 90

        gameScene.state = "finish"
    }

    function die(){ //when the player hit a wall
        playerEntity.x = gameScene.width / 2
        playerEntity.y = 90

        gameScene.score =0
        gameScene.state = "gameOver"
    }


}
